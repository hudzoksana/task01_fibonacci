package com.hudz.task01;

import java.util.HashMap;
import java.util.Map;

/** Class for operations with Fibonacci numbers.
 * @author Hudz Oksana
 * @version 1.0
 */

public class Fibonacci {
    /**
     * Max percentage value is 100.
     */
    private final static int MAXPERCENTAGE = 100;

    /**
     * Collection for Fibonacci numbers.
     */
    private Map<Integer, Integer> values = new HashMap<Integer, Integer>();

    /** Method for getting size of the Fibonacci numbers set.
     * @return size of set values
     */
    private int getSize() {
        return values.size();
    }

    /** Method for getting
     * value of the Fibonacci numbers element with index i.
     * @param i - index of element in set.
     * @return value of element with index i
     */
    private int getFibonacci(final int i) {
        return values.get(i);
    }

    /** Method for putting information into Fibonacci set.
     * @param firstNumber first number for Fibonacci set
     * @param secondNumber  second number for Fibonacci set
     * @param size  amount of elements in Fibonacci set
     */

    public final void buildFibonacci(final int firstNumber,
                                     final int secondNumber,
                                     final int size) {

        values.put(0, firstNumber);
        values.put(1, secondNumber);

        for (int i = 2; i < size; i++) {
            values.put(i, values.get(i - 1) + values.get(i - 2));
        }
    }

    /** Method for printing numbers from the param values.
     * @param values  set of the Fibonacci numbers
     */

    public static void printFibonacci(final Fibonacci values) {

        for (int i = 0; i < values.getSize(); i++) {
            System.out.println("F[" + (i + 1) + "]=" + values.getFibonacci(i));
        }
    }

    /** Method for printing percentage of odd and even
     * Fibonacci numbers.
     * @param values  set of the Fibonacci numbers
     */

    public static void printPercentageOddEven(final Fibonacci values) {

        double percentage;
        int countOdd = 0;
        int countEven = 0;


        percentage = MAXPERCENTAGE / values.getSize();

        for (int i = 0; i < values.getSize(); i++) {
            if (values.getFibonacci(i) % 2 == 0) {
                countEven++;
            } else {
                countOdd++;
            }
        }

        System.out.println("Percentage of odd Fibonacci numbers is "
                + (percentage * countOdd) + "% "
                +  "and even " + (percentage * countEven) + "%");
    }
}
