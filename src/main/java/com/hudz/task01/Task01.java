package com.hudz.task01;

import java.util.Scanner;

/** Class with main for task01_Fibonacci.
 * @author  Hudz Oksana
 * @version 1.0

 */
public abstract class Task01 {

    /** Variable for checking interval [1;100].
     */
    final static int ONE = 1;
    /** Variable for checking interval [1;100].
     */
    final static int HUNDRED = 100;
    /** Main function for the task01.
     * @param args command line arguments
     */
    public static void main(String[] args) {

        int startOfInterval = 0;
        int endOfInterval = 0;
        int sumOdd = 0;
        int sumEven = 0;
        int sumEvenOdd;
        int biggestOdd;
        int biggestEven;
        int sizeOfSet;
        Fibonacci fibonacci = new Fibonacci();

        //getting interval from a user

        boolean check = true;

        while (check) {

            System.out.print("Enter the start number [1; 100]: ");
            startOfInterval = getNumber();

            System.out.print("Enter the finish number [1; 100]: ");
            endOfInterval = getNumber();

            if (startOfInterval > endOfInterval) {
                System.out.println("Start number should be less then finish! ");
            } else {
                check = false;
            }

        }

        //printing odd numbers from start to the end

        System.out.println("\nOdd numbers from start to the end:");
        for (int i = startOfInterval; i <= endOfInterval; i++) {
            if (i % 2 == 1) {
                System.out.print(i + " ");

                //counting sum of odd numbers
                sumOdd += i;
            }

        }

        System.out.println();

        //printing even numbers from end to start

        System.out.println("\nEven numbers from end to start:");
        for (int i = endOfInterval; i >= startOfInterval; i--) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
                //counting sum of even numbers
                sumEven += i;
            }
        }

        sumEvenOdd = sumOdd + sumEven;

        System.out.println("\n\nSum of odd and even numbers is: " + sumEvenOdd);

        System.out.println("\nPart with Fibonacci numbers:\n");

        System.out.print("Please input size of set N: ");

        sizeOfSet = getNumber();

        //setting biggestEven and biggestOdd numbers

        if (endOfInterval % 2 == 0) {

            biggestEven = endOfInterval;
            biggestOdd = endOfInterval - 1;

        } else {

            biggestEven = endOfInterval - 1;
            biggestOdd = endOfInterval;

        }

        System.out.println("\nBiggest odd number (F1) is " + biggestOdd);

        System.out.println("\nBiggest even number (F2) is " + biggestEven);


        fibonacci.buildFibonacci(biggestOdd, biggestEven, sizeOfSet);

        System.out.println("Set of Fibonacci numbers:");
        Fibonacci.printFibonacci(fibonacci);

        Fibonacci.printPercentageOddEven(fibonacci);

    }

    /** Method for getting input number from 1 to 100 from the command line.
     * @return number from the command line from [1;100]
     */
    public static int getNumber() {

        int number = 0;
        Scanner sc = new Scanner(System.in);
        boolean check;

        do {
            check = true;
            if (!sc.hasNextInt()) {
                System.out.print("Please enter the number: ");
                sc.next();
                check = false;
            } else {
                number = sc.nextInt();
                if (number < ONE || number > HUNDRED) {
                    System.out.print("Number should be between 0 and 100: ");
                    check = false;
                }
            }
        } while (!check);

        return number;
    }



}
